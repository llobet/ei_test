'use strict';



const pathImg = '../../../assets/images/';

//HOME
import elementsLogoSvg from '../../../assets/images/Projects/elements-logo.svg';
import elementsLogo2x from '../../../assets/images/Projects/elements-logo@2x.png';
import elementsLogo from '../../../assets/images/Projects/elements-logo.png';

//ABOUT
import bcnCity2x from '../../../assets/images/About/bcn-city@2x.png';
import bcnCity from '../../../assets/images/About/bcn-city.png';
import almereCity2x from '../../../assets/images/About/almere-city@2x.png';
import almereCity from '../../../assets/images/About/almere-city.png';

//BLOG
import developersAnalyzing2x from '../../../assets/images/Blog/developers-analyzing@2x.jpg';
import developersAnalyzing from '../../../assets/images/Blog/developers-analyzing.jpg';

import deviceLabTesting2x from '../../../assets/images/Blog/device-lab-testing@2x.jpg';
import deviceLabTesting from '../../../assets/images/Blog/device-lab-testing.jpg';

//BLOG -> PARALLAX
import androidLogoSvg from '../../../assets/images/Parallax/android-logo.svg';
import androidLogo2x from '../../../assets/images/Parallax/android-logo@2x.png';
import androidLogo from '../../../assets/images/Parallax/android-logo.png';
import iosLogoSvg from '../../../assets/images/Parallax/IOS-logo.svg';
import iosLogo2x from '../../../assets/images/Parallax/IOS-logo@2x.png';
import iosLogo from '../../../assets/images/Parallax/IOS-logo.png';
import windowsLogoSvg from '../../../assets/images/Parallax/windows-logo.svg';
import windowsLogo2x from '../../../assets/images/Parallax/windows-logo@2x.png';
import windowsLogo from '../../../assets/images/Parallax/windows-logo.png';
import iphoneDeviceSvg from '../../../assets/images/Parallax/iphone-device.svg';
import iphoneDevice2x from '../../../assets/images/Parallax/iphone-device@2x.png';
import iphoneDevice from '../../../assets/images/Parallax/iphone-device.png';
import nokiaDeviceSvg from '../../../assets/images/Parallax/nokia-lumia-device.svg';
import nokiaDevice2x from '../../../assets/images/Parallax/nokia-lumia-device@2x.png';
import nokiaDevice from '../../../assets/images/Parallax/nokia-lumia-device.png';

function MainController($log, $scope, $http, parallaxHelper, $ocLazyLoad) {
  'ngInject';

 	$log.debug('Hello from main controller!');

 	//carousel GET
 	$http.get($scope.api('carousel'), $scope.headers)
	.then(function(response) {
	    $scope.carousel = response.data;
	});


	// comments GET & POST
	$scope.submitComment = (isValid) => {
		$scope.submittedComment = true;
		if(isValid){
			$http.post($scope.api('comments'),$scope.comment, $scope.headers)
			.then(function(response) {
				$log.debug(response);
			    $scope.comments.results.push(response.data);
			});
		}
	}
	

	$http.get($scope.api('comments'), $scope.headers)
	.then(function(response) {
	    $scope.comments = response.data;
	});


	// company GET
	$http.get($scope.api('company'), $scope.headers)
	.then(function(response) {
		$scope.company = response.data;
	});


	// message POST
	$scope.submitContact = (isValid) => {
		$scope.submittedContact = true;
		if(isValid){
			$http.post($scope.api('message'),$scope.contact, $scope.headers)
			.then(function(response) {
				$log.debug(response);
			});
		}
	}


	//parallax
	$scope.iosLogoX = parallaxHelper.createAnimator(-0.18);
	$scope.iosLogoY = parallaxHelper.createAnimator(-0.1);

	$scope.androidLogoX = parallaxHelper.createAnimator(-0.05);
	$scope.androidLogoY = parallaxHelper.createAnimator(-0.12);

	$scope.windowsLogoX = parallaxHelper.createAnimator(-0.07);
	$scope.windowsLogoY = parallaxHelper.createAnimator(0.16);

	$scope.iphoneDeviceX = parallaxHelper.createAnimator(0.14);
	$scope.iphoneDeviceY = parallaxHelper.createAnimator(0.09);

	$scope.nokiaDeviceX = parallaxHelper.createAnimator(-0.14);
	$scope.nokiaDeviceY = parallaxHelper.createAnimator(0.09);


	//images
	this.elementsLogoSvg = elementsLogoSvg;
    this.elementsLogo2x = elementsLogo2x;
    this.elementsLogo = elementsLogo;
    
    this.bcnCity2x = bcnCity2x;
    this.bcnCity = bcnCity;
    this.almereCity2x = almereCity2x;
    this.almereCity = almereCity;

    this.developersAnalyzing2x = developersAnalyzing2x;
    this.developersAnalyzing = developersAnalyzing;
    this.deviceLabTesting2x = deviceLabTesting2x;
    this.deviceLabTesting = deviceLabTesting;

	this.androidLogoSvg = androidLogoSvg;
    this.androidLogo2x = androidLogo2x;
	this.androidLogo = androidLogo;
	this.iosLogoSvg = iosLogoSvg;
	this.iosLogo2x = iosLogo2x;
	this.iosLogo = iosLogo;
	this.windowsLogoSvg = windowsLogoSvg;
	this.windowsLogo2x = windowsLogo2x;
	this.windowsLogo = windowsLogo;
	this.iphoneDeviceSvg = iphoneDeviceSvg;
	this.iphoneDevice2x = iphoneDevice2x;
	this.iphoneDevice = iphoneDevice;
	this.nokiaDeviceSvg = nokiaDeviceSvg;
	this.nokiaDevice2x = nokiaDevice2x;
	this.nokiaDevice = nokiaDevice;


}

export default MainController;
