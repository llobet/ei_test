'use strict';

import route from './main.route';

const mainPageModule = angular.module('main-module', [
  'ui.router',
  'duParallax',
  "oc.lazyLoad"
]);

mainPageModule
	.run(function($rootScope) {
	    $rootScope.api = function(path) {return `https://recruitment.elements.nl:8080/v1/${path}/`;}
		$rootScope.token = '41d60fd4acdcaa2e731972607a52dee2ac4bf0b1';
		$rootScope.headers = {headers:{'Authorization':`Token ${$rootScope.token}`}};
	});


mainPageModule
    .config(route);

export default mainPageModule;
