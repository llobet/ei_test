'use strict';

import footerTpl from './footer.html';
import FooterController from './footer.controller';

function footerComponent($log) {
	'ngInject';

  const directive = {
    restrict: 'E',
    templateUrl: footerTpl,
    controller: FooterController,
    controllerAs: 'vm',
    bindToController: true
  };

  return directive;

}

export default footerComponent;
